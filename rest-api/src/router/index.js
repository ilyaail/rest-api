import Vue from 'vue';
import VueRouter from 'vue-router';
import Hello from '@/views/Hello';
import user from '@/views/user';
Vue.use(VueRouter);
const routes = [
  {
    path: '/',
    name: 'Hello',
    component: Hello,
  },
  {
    path: '/users',
    name: 'user',
    component: user,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});
export default router;
