const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const usersRouter = require('./routes/userRouter');
const db = require('./database');

db.createOrOpenDatabase()
  .then(() => {
    console.log('Database was connected');
    app.listen(8080, () => {
      console.log('database started listen at 8080 port...');
    });
  })
  .catch((e) => {
    console.error(e.message);
    process.exit(-1);
  });

app.use('/users', usersRouter);
