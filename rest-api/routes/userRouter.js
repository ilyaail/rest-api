const express = require('express');
const router = express.Router();
const getUsersController = require('../controllers/userController');

router.get('/', getUsersController.getAllUsers);
router.get('/find/:name?', getUsersController.findByUser);
router.post('/create', getUsersController.createUser);
router.delete('/remove', getUsersController.removeUser);
router.put('/update', getUsersController.updateUser);

module.exports = router;
