const db = require('../database');

class userFinder {
  async find(params) {
    const result = await db.findByUser(params);
    return result;
  }
}

module.exports = userFinder;
