const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: String,
  position: String,
  age: Number,
});

module.exports = mongoose.model('Users', userSchema);
