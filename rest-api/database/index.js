const mongoose = require('mongoose');
const Users = require('./Schema/index');
module.exports = Object.freeze({
  createOrOpenDatabase,
  createUser,
  findUser,
  updateUser,
  removeUser,
  findByUser,
});

async function createOrOpenDatabase() {
  const url = 'mongodb://localhost:27017/usersvuedb';
  return mongoose.connect(url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  });
}
async function findByUser(params) {
  const result = await Users.findOne(params);
  console.log('result ' + result);
  if (!result) {
    return null;
  }
  return result;
}
async function findUser() {
  const result = await Users.find();
  return result;
}

async function createUser(params) {
  const result = await Users.create(params);
  if (!result) {
    return null;
  }
  return result;
}

async function updateUser(params) {
  const result = await Users.findOneAndUpdate({ _id: params.id }, params);
  if (!result) {
    return null;
  }
  return result;
}

async function removeUser(_id) {
  return Users.findOneAndDelete({ _id });
}
